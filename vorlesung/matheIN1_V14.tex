\documentclass[a4paper,12pt]{article}
\usepackage{amsmath, amsfonts, amssymb, float, enumerate, fancyhdr, graphicx, fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{unicode-math}

\usepackage[cm,headings]{fullpage}

\graphicspath{{./images/}}

\author{Anna Brede}
\title{Mathe für Informatiker 1 - Jens Schreyer - Vorlesung 14}
\date{2022-11-28}

\makeatletter

\pagestyle{fancy}
\fancyhf{}
\rhead{\@date}
\lhead{\@title}
\lfoot{\@author}
\rfoot{\thepage}
\parindent 0pt
\renewcommand{\footrulewidth}{0.4pt}

\begin{document}

\includegraphics[width=80mm]{v14_Bild1.png}
\vspace{10pt}
   
\paragraph{(3) Lokale Extremwerte, notwendige Bedingung}
\hspace{0pt}\vspace{10pt}

\underline{Vor.:} $f$ ist in $x₀$ differenzierbar
\vspace{10pt}

\underline{Beh.:} $\boxed{x₀\text{ ist lokale Maximal-/Minimalstelle }⇒f'(x₀)=0}$
\vspace{10pt}

\underline{Beweis} (für lokales Maximum, d.h. $f(x₀)=\underset{x∈I₀}{max}f(x)\quad I₀=(x₀-ε,\;x₀+ε)$)
\vspace{10pt}

\includegraphics[width=80mm]{v14_Bild2.png}
\vspace{10pt}

$$Δf(x₀,h)=f(x₀+h)-f(x₀)\leq0\text{ für alle }h\text{ mit }|h|<ε$$

$$⇒\frac{Δf(x₀,h)}{h}\left\{\begin{matrix}
    \geq0&\text{ für }-ε<h<0\\
    \leq0&\text{ für }0<h<ε
\end{matrix}\right.$$

$$⇒f'(x₀)=\lim_{h→0}\frac{Δf(x₀,h)}{h}=0\qquad\square$$

\underline{Folgerung aus dem Satz von Weierstraß}
\vspace{10pt}

Sei $f$ stetig auf $I=[a,b]$ und differenzierbar auf $(a,b)$. Sei

$$B=\{f(a),f(b)\}\cup\{f(x)|x∈(a,b),\;f'(x)=0\}$$

Dann gilt $\underset{x∈I}{max}f(x)=maxB\quad\underset{x∈I}{min}f(x)=minB$
\pagebreak

\underline{Bezeichnung}
\vspace{10pt}

Ist $f'(x)=0$ so heißt $(x₀,f(x₀))$ \underline{stationärer Punkt} und $x₀$ \underline{extremwertverdächtige Stelle}.
\vspace{10pt}

\underline{Bemerkung}
\vspace{10pt}

Zur Bestimmung globaler Maxima/Minima braucht man keine 2. Ableitung
\vspace{10pt}

\includegraphics[width=80mm]{v14_Bild3.png}
\vspace{10pt}

\paragraph{\underline{(4) Lokale Extremwerte, hinreichende Bedingung}}
\hspace{0pt}\vspace{10pt}

$x₀$ ist lokale Maximal- bzw. Minimalstelle, falls eine der folgenden Bedingungen erfüllt ist
\vspace{10pt}

\underline{Typ 1}
\vspace{10pt}

\begin{enumerate}[(1)]
    \item $f'(x₀)=0$
    \item $∃ε>0$ derart, dass gilt
    $$\underset{x₀\text{ lokale Minimalstelle}}{\underbrace{f'(x)\left\{\begin{matrix}
        <0&\text{ für }x∈(x₀-ε,x₀)\\
        >0&\text{ für }x∈(x₀,x₀+ε)
    \end{matrix}\right.}}\qquad\qquad\underset{x₀\text{ lokale Maximalstelle}}{\underbrace{f'(x)\left\{\begin{matrix}
        >0&x∈(x₀-ε,x₀)\\
        <0&x∈(x₀,x₀+ε)
    \end{matrix}\right.}}$$
\end{enumerate}
\vspace{10pt}

Beweis für lokale Maximalstelle
\vspace{10pt}

\includegraphics[width=80mm]{v14_Bild4.png}
\vspace{10pt}

$f'(x)>0$ für $x∈(x₀-ε,x₀)⇒f$ (streng) monoton wachsend für $x∈(x₀-ε,x₀]$
\vspace{10pt}

$⇒\underset{x∈(x₀-ε,x₀]}{max}f(x)=f(x₀)$
\vspace{10pt}

$f'(x)<0$ für $x∈(x₀,x₀+ε)⇒f$ monoton fallend für $x∈[x₀,x₀+ε)$
\vspace{10pt}

$⇒\underset{x∈[x₀,x₀+ε)}{max}f(x)=f(x₀)$
\vspace{10pt}

$⇒\underset{x∈(x₀-ε,x₀+ε)}{max}f(x)=f(x₀)\qquad\square$
\vspace{10pt}

Bemerkung: Betrachtung ist auch gültig, falls $f$ in $x₀$ nicht differenzierbar ist.
\vspace{10pt}

\includegraphics[width=40mm]{v14_Bild5.png}
\vspace{10pt}

\underline{Typ II}
\vspace{10pt}

Ist die Funktion $f$ in einer Umgebung von $x₀$ 2mal stetig differenzierbar und gilt

$$\left.\begin{matrix}
    f'(x₀)=0\\
    f''(x₀)>0
\end{matrix}\right\}⇒x₀\text{ ist Minimalstelle}\quad\left.\begin{matrix}
    f'(x₀)=0\\
    f''(x₀)<0
\end{matrix}\right\}⇒x₀\text{ ist Maximalstelle}$$

\underline{Bew.} (für Maximalstelle)
\vspace{10pt}

\begin{itemize}
    \item $f''(x₀)<0$ und $f''$ stetig $⇒∃ε>0:\;f''(x)<0$ für $x∈(x₀-ε,x₀+ε)$
    \item[$⇒$] $f'$ ist streng monoton fallend auf $(x₀-ε,x₀+ε)$ (und $f'(x₀)=0$)
    \item[$⇒$] $f'(x)>0$ für $x∈(x₀-ε,x₀),\;f'(x)<0$ für $x∈(x₀,x₀+ε)\overset{\text{Typ I}}{⇒}x₀$ ist lokale Maximalstelle 
\end{itemize}
\vspace{10pt}

\paragraph{\underline{(6.5) Krümmung, Wendepunkte}}
\hspace{0pt}\vspace{10pt}

\includegraphics[width=100mm]{v14_Bild6.png}
\vspace{10pt}

\includegraphics[width=100mm]{v14_Bild7.png}
\vspace{10pt}

$$x∈(x₁,x₂)⇔\begin{matrix}
    x=x₁+α(x₂-x₁)\\
    x=(1-α)x₁+αx₂
\end{matrix}\quad α∈(0,1)\quad\text{(Konvexkombination von $x₁$, $x₂$)}$$

\begin{align*}
    S(x)&=f(x₁)+(x-x₁)·\frac{f(x₂)-f(x₁)}{x₂-x₁}\\
    &=f(x₁)+α(x₂-x₁)·\frac{f(x₂)-f(x₁)}{x₂-x₁}\\
    S(x)&\underline{=f(x₁)+α(f(x₂)-f(x₁))}\\
    &=(1-α)f(x₁)+αf(x₂)
\end{align*}

\underline{(1) Definition}
\vspace{10pt}

Die Funktion $f$ heißt $\left\{\begin{matrix}
    \text{\underline{konvex}}\\
    \text{\underline{konkav}}
\end{matrix}\right\}$ bzw. \underline{streng} $\left\{\begin{matrix}
    \text{\underline{konvex}}\\
    \text{\underline{konkav}}
\end{matrix}\right\}$ auf dem Intervall $I$, falls $∀x₁,x₂∈I$ mit $x₁<x₂$ gilt.

$$f(x₁+a(x₂-x₁))\left\{\begin{matrix}
    \leq\\
    \geq
\end{matrix}\right\}\;f(x₁)+α(f(x₂)-f(x₁))\quad\text{für alle }α∈(0,1)$$

bzw.

$$f(x₁+α(x₂-x₁))\left\{\begin{matrix}
    <\\
    >
\end{matrix}\right\}\;f(x₁)+α(f(x₂)f(x₁))$$

\underline{Kriterium}
\vspace{10pt}

Sei $f$ stetig auf $[a,b]$ und 2mal stetig differenzierbar auf $(a,b)$. Dann gilt
\vspace{10pt}

\begin{enumerate}[a)]
    \item $f$ (streng) konvex bzw. (streng) konkav auf $I$\\
    $⇔$\\
    $f'$ (streng) monoton wachsend bzw. $f'$ (streng) monoton fallend auf $I$
    \item $f''(x)>0\;∀x∈(a,b)⇒f$ streng konvex auf $I$
    \item $f''(x)<0\;∀x∈(a,b)⇒f$ streng konkav auf $I$
\end{enumerate}
\pagebreak

\underline{Bsp.:}
\vspace{10pt}

$f(x)=x³,\;f'(x)=3x²,\;f''(x)=6x$

$$f''(x)\left\{\begin{matrix}
    <0&x<0\\
    >0&x>0
\end{matrix}\right.$$

\includegraphics[width=80mm]{v14_Bild8.png}
\vspace{10pt}

$f$ streng konkav für $x∈(-∞,0]$
\vspace{10pt}

$f$ streng konvex für $x∈[0,∞)$
\vspace{10pt}

\underline{(3) Satz}
\vspace{10pt}

Sei $I=[a,b]$ abgeschlossenes Intervall und sei $f$ stetig auf $I$ so gilt
\vspace{10pt}

Ist $f$ streng konvex auf $I$, dann ist

$$\underset{x∈I}{max}f(x)=max\{f(a),f(b)\}$$

Ist $f$ streng konkav auf $I$, dann ist

$$\underset{x∈I}{min}f(x)=min\{f(a),f(b)\}$$

\underline{Beispiel}
\vspace{10pt}

$f(x)=e^x-\ln x+x\quad I=[1,2]$
\vspace{10pt}

\begin{itemize}
    \item $f$ stetig auf $I$
    \item $f'(x)=e^x-\frac{1}{x}+1,\;f''(x)=e^x+\frac{1}{x²}$
    \item $f''(x)=e^x+\frac{1}{x²}>0∀x∈I⇒f$ streng konvex auf $I$
\end{itemize}
\vspace{10pt}

$\overset{\text{Satz}}{⇒}\underset{x∈I}{max}f(x)=max\{f(1),f(2)\}=max\{e+1,e²-\ln 2+2\}$
\vspace{10pt}

$=e²-\ln 2+2=f(2)$
\vspace{10pt}

\begin{itemize}
    \item $f''(x)>0∀x∈I$
    \item[$⇒$] $f'$ ist streng monoton wachsend auf $I$
    \item[] $f'(1)=e>0⇒f'(x)\geq e>0∀x∈I$
    \item[$⇒$] $f$ streng monoton wachsend auf $I$ 
    \item[$⇒$] $\underset{x∈I}{min}f(x)=f(1)=e+1$ 
\end{itemize}
\vspace{10pt}

\underline{Bemerkung} schwieriger für $I=[\frac{1}{10},2]$

$$f'(\frac{1}{10})<0,\;f'(2)>0\overset{\text{ZWS}}{⇒}∃x₀∈I',\;f'(x₀)=0$$

\underline{(4) Definition}
\vspace{10pt}

$x₀$ ist \underline{Wendestelle} von $f$, falls $f$ an der Stelle $x₀$ bzgl. einer Umgebung $(x₀-ε,x₀+ε)$ sein Komplexitätsverhalten ändert.
\vspace{10pt}

$(x₀,f(x₀))$ heißt dann \underline{Wendepunkt} von $f$.
\vspace{10pt}

\underline{(5) Wendepunkttest}
\vspace{10pt}

\underline{Vor.:} $f$ ist in Umgebung von $x₀$ differenzierbar (evtl. mehrfach)
\vspace{10pt}

\underline{Satz}
\vspace{10pt}

\begin{enumerate}[(a)]
    \item $x₀$ Wendestelle von $f⇔x₀$ ist lokale Maximal-/Minimalstelle von $f'$
    \item \underline{notwendige Bedingung (falls $f$ zweimal stetig differenzierbar)}
    $$x₀\text{ Wendestelle von }f⇒f''(x₀)=0$$
    \item \underline{hinreichende Bedingung (falls $f$ 3mal stetig differenzierbar)}
    $$x₀\text{ Wendestelle von }f\text{ falls gilt }f''(x₀)=0\quad f'''(x₀)\neq0$$
\end{enumerate}
\vspace{10pt}

\underline{(6) Satz}
\vspace{10pt}

Sei $f'(x₀)=f''(x₀)=…=f^{(n-1)}(x₀)=0,\;f^{(n)}(x₀)\neq0$ mit $n\geq2$. Dann gilt
\vspace{10pt}

\begin{enumerate}[a)]
    \item $x₀$ Wendestelle von $f⇔n$ ungerade
    \item $x₀$ lokale Minimalstelle von $f⇔n$ gerade, $f^{(n)}(x₀)>0$
    \item $x₀$ lokale Maximalstelle von $f⇔n$ gerade, $f^{(n)}(x₀)<0$
\end{enumerate}
\vspace{10pt}

Begründung für den Satz kommt später
\pagebreak

\underline{Bsp.:}
\vspace{10pt}

$f(x)=x^4$
\vspace{10pt}

\includegraphics[width=80mm]{v14_Bild9.png}
\vspace{10pt}

\begin{align*}
    f'(x)&=4x³&\quad f'(0)&=0\\
    f''(x)&=12x²&\quad f''(0)&=0\\
    f'''(x)&=24x&\quad f'''(0)&=0\\
    f^{(4)}(x)&=24&\quad f^{(4)}(0)&=24>0⇒f(0)&\text{ ist lokale Minimalstelle}\\
    &&&&\text{sogar globale Minimalstelle auf }I=ℝ
\end{align*}




\end{document}