\documentclass[a4paper,12pt]{article}
\usepackage{amsmath, amsfonts, amssymb, float, enumerate, fancyhdr, graphicx, fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{unicode-math}

\usepackage[cm,headings]{fullpage}

\graphicspath{{./images/}}

\author{Anna Brede}
\title{Mathe für Informatiker 1 - Jens Schreyer - Vorlesung 16}
\date{2022-12-05}

\makeatletter

\pagestyle{fancy}
\fancyhf{}
\rhead{\@date}
\lhead{\@title}
\lfoot{\@author}
\rfoot{\thepage}
\parindent 0pt
\renewcommand{\footrulewidth}{0.4pt}

\begin{document}
   
$f(x₀),\;f'(x₀),\;f''(x₀),…,f^{(n)}(x₀)$

$$T_n(x)=\sum_{k=0}^n\frac{f^{(k)}(x₀)}{k!}(x-x₀)^k$$

$$f(x)-T_n(x)=R_n(x)=\frac{f^{(n+1)}(\tilde{x})}{(n+1)!}(x-x₀)^{n+1}$$

$$T_{2n+1}(x)=\sum_{k=0}^n\frac{(-1)^k}{(2k+1)!}(x-x₀)^k\quad T_{2n}=T_{2n-1}$$

$$|R_n(x)|\leq\frac{|x|}{(n+1)!}^{n+1}$$

\includegraphics[width=100mm]{v16_Bild1.png}
\vspace{10pt}

\underline{Beispiel 2}
\vspace{10pt}

$f(x)=e^x,\;x₀=0$
\vspace{10pt}

\begin{itemize}
    \item $f^{(k)}(x)=e^x,\;f^{(k)}(0)=e^0=1$
    \item \begin{align*}
        T(x)&=\sum_{k=0}^∞\frac{f^{(k)'}(0)}{k!}(x-x₀)^k\\
        T(x)&=\sum_{k=0}^∞\frac{1}{k!}x^k
    \end{align*}
    \item $$R_n(x)=\frac{f^{(n+1)}(\tilde{x})}{(n+1)!}(x-x₀)^{n+1}$$
    $x$ zwischen $x₀$ und $x$
\end{itemize}

$$f^{(n+1)}(\tilde{x})=e^{\tilde{x}}\left\{\begin{matrix}
    <e^x&\text{für }0<\tilde{x}<x\\
    <e^0&\text{für }x<\tilde{x}<0
\end{matrix}\right.$$

$$|R_n(x)|=\frac{\left|f^{(n+1)}(\tilde{x})\right|}{(n+1!)}\left|x^{n+1}\right|\leq\left\{\begin{matrix}
    e^x·\frac{|x|^{n+1}}{(n+1)!}&,\;x>0\\
    1·\frac{|x|^{n+1}}{(n+1)!}&,\;x<0
\end{matrix}\right.$$

$$\overset{\lim_{n→∞}\frac{|x|^{n+1}}{(n+1)!}=0}{⇒}\lim_{n→∞}|R_n|=0$$

Also gilt $f(x)=T(x)∀x∈ℝ$, d.h.

$$e^x=\sum_{k=0}^∞\frac{x^k}{k!}$$

$→$ eigentlich ist das die Definition der $e$-Funktion
\vspace{10pt}

\underline{Beispiel}
\vspace{10pt}

$$f(x)=\left\{\begin{matrix}
    e^{-\frac{1}{x²}}&\text{für }x\neq0\\
    0&\text{für }x=0
\end{matrix}\right.$$

\includegraphics[width=100mm]{v16_Bild2.png}
\vspace{10pt}

$f^{(k)}(0)=0$ (lange Rechnung)

$$⇒T(x)=\sum_{k=0}^∞\frac{f^{(k)}(0)}{k!}x^k=0$$

(Nullfunktion)
\vspace{10pt}

$⇒T(x)=f(x)$ nur bei $x=x₀=0$
\vspace{10pt}

\paragraph{\underline{(7.3) Potenzreihen}}
\hspace{0pt}\vspace{10pt}

\underline{(1) Definition}
\vspace{10pt}

Eine Reihe der Form

$$\sum_{k=0}^∞a_k(x-x₀)^k=a₀+a₁(x-x₀)^1+a₂(x-x₀)²+…$$

wird \underline{Potenzreihe}(PR) genannt. Dann heißt\begin{itemize}
    \item $x₀$ \underline{Zentrum/Entwicklungsstelle} der PR
    \item $a_k$ Koeffizienten der PR
    \item $x$ Unbekannte der PR
\end{itemize}
\vspace{10pt}

\underline{Bsp.:}

\begin{itemize}
    \item $$\sum_{k=0}^∞x^k\quad(x₀=0,\;a_k=1\;∀k)$$
    Reihe konvergent für $|x|<1$ und $\sum_{k=0}^∞x^k=\frac{1}{1-x}$ (geometrische Reihe)
    \item Jede Taylorreihe ist eine Potenzreihe
    $$\left(a_k=\frac{f^{(k)}(x₀)}{k!}\right)$$
\end{itemize}
\vspace{10pt}

\underline{(2) Konvergenzverhalten}
\vspace{10pt}

Es gibt immer ein Intervall $I$ der Form

$$I=(x₀-r,x₀+r)=\{x∈ℝ||x-x₀|<r\}$$

so, dass gilt

$$\sum_{k=0}^∞a_k(x-x₀)^k\text{ ist }\left\{\begin{matrix}
    \text{absolut konvergent }&\text{für }x∈I(|x-x₀|<r)\\
    \text{divergent }&|x-x₀|>r\\
    ?&x=x₀\pm r
\end{matrix}\right.$$

Man nennt dann $I$ das \underline{Konvergenzintervall} der PR und $r$ den \underline{Konvergenzradius}.
\vspace{10pt}

\includegraphics[width=70mm]{v16_Bild3.png}
\vspace{10pt}

Dann existiert für alle $x∈I$ die Summe

$$f(x):=\sum_{k=0}^∞a_k(x-x₀)^k$$

Man nennt $f:I→ℝ$ die \underline{Summenfunktion} der PR
\vspace{10pt}

\underline{(3) Bestimmung von $I$ mit Wurzel-/Quotientenkriterium}
\vspace{10pt}

$$q(x)=\lim_{k→∞}\sqrt[k]{|a_k(x-x₀)^k|}=\lim_{k→∞}\left|\frac{a_{k+1}(x-x₀)^{k+1}}{a_k(x-x₀)^k}\right|$$

Dann gilt
\vspace{10pt}

PR ist für $x\left\{\begin{matrix}
    \text{ absolut konvergent }&q(x)<1\\
    \text{divergent }&q(x)>0\\
    ?&q(x)=1
\end{matrix}\right.$
\pagebreak

\underline{Beispiel}

$$\sum_{k=1}^∞\frac{1}{k}x^k=x+\frac{x²}{2}+\frac{x^3}{3}+\frac{x^4}{4}+…$$

\begin{itemize}
    \item $$q(x)=\lim_{k→∞}\left|\frac{\frac{1}{k+1}x^{k+1}}{\frac{1}{k}x^k}\right|=\lim_{k→∞}\underset{→1}{\underbrace{\frac{k}{k+1}}}·|x|=|x|$$
    \item $q(x)<1⇔|x|<1⇔-1<x<1$
    \item Konvergenzintervall $I=(-1,1)$
\end{itemize}
\vspace{10pt}

Summenfunktion

$$f(x)=\sum_{k=1}^∞\frac{1}{k}x^k,\;f:(-1,1)→ℝ$$

Betrachtung der Randpunkte $-1,1$
\vspace{10pt}

$x=1:\;\sum_{k=1}^∞\frac{1}{k}→$ PR divergent (harmonische Reihe)
\vspace{10pt}

$x=-1:\;\sum_{k=1}^∞\frac{(1)^k}{k}→$ PR konvergent (alternierende harmonische Reihe)
\vspace{10pt}

\underline{Beispiel 2}

$$\sum_{k=0}^∞\frac{2^k}{3k+1}(x-1)^{2k}\quad a_l=\left\{\begin{matrix}
    0&l=2k+1\\
    \frac{2^k}{3k+1}&l=2k
\end{matrix}\right.$$

Quotientenkriterium

$$\left|\frac{\frac{2^{k+1}}{3(k+1)+1}(x-1)^{2(k+1)}}{\frac{2^k}{3k+1}(x-1)^{2k}}\right|=\left|\frac{\overset{=2·2^k}{2^{k+1}}}{3k+4}·\frac{3k+1}{2^k}·\frac{(x-1)^{2k+2}}{(x-1)^{2k}}\right|=2·\frac{3k+1}{3k+4}·(x-1)²\overset{k→∞}{→}2·(x-1)²\overset{!}{<}1$$

\begin{align*}
    (x-1)²&<\frac{1}{2}\\
    |x-1|&<\frac{1}{\sqrt{2}}\\
    ⇔-\frac{1}{\sqrt{2}}&<x-1<\frac{1}{\sqrt{2}}\\
    1-\frac{1}{\sqrt2}&\leq x\leq1+\frac{1}{\sqrt2}
\end{align*}

$$I=\left(1-\frac{1}{\sqrt2},1+\frac{1}{\sqrt2}\right)$$

\paragraph{\underline{(7.4) Rechnen mit Potenzreihen}}
\hspace{0pt}\vspace{10pt}

Seien

\begin{align*}
    f(x)&=\sum_{k=0}^∞a_k(x-x₀)^k\;∀x∈I₁\\
    g(x)&=\sum_{k=0}^∞b_k(x-x₀)^k\;∀x∈I₂
\end{align*}

2 Potenzreihen mit dem gleichen Zentrum. Dann gilt für die Summenfunktionen
\vspace{10pt}

\begin{enumerate}[(1)]
    \item $$α·f(x)+β·g(x)=\sum_{k=0}^∞(αa_k+βb_k)(x-x₀)^k\;∀x∈I₁\cap I₂$$
    \item $$f(x)·g(x)=\sum_{k=0}^∞(a₀b_k+a₁b_{k-1}+…+a_kb₀)(x-x₀)^k\;∀x∈I₁\cap I₂$$
    (vgl. Cauchy-Produkt von Reihen)
    \item $f$ ist stetig differenzierbar auf dem Konvergenzintervall $I₁$ und die Ableitung kann gliedweise gebildet werden, d.h.
    $$f'(x)=\sum_{k=0}^∞[a_k(x-x₀)^k]'=\sum_{k=1}^∞a_k·k(x-x₀)^{k-1}\;∀x∈I₁$$
    \item $f$ ist beliebig oft differenzierbar auf $I₁$ und es gilt
    $$f^{(k)}(x₀)=k!a_k\text{ bzw }a_k=\frac{f^{(k)}(x₀)}{k!}$$
    →Begründung analog zu Polynomen
    ⇒$f$ stimmt auf $I$ mit der Taylorreihe von $f$ überein
\end{enumerate}
\vspace{10pt}

\underline{Bsp.:} Summenfkt.

$$f(x)=\sum_{k=1}^∞\frac{1}{k}·x^k,\;I=(-1,1)$$

\begin{itemize}
    \item \begin{align*}
        f'(x)&=\sum_{k=1}^∞\left(\frac{1}{k}·x^k\right)'=\sum_{k=1}^∞x^{k-1}=1+x+x²+…\\
        f'(x)&=\sum_{k=0}^∞x^k=\frac{1}{1-x}
    \end{align*}
    \begin{align*}
        ⇒f(x)&=-\ln|1-x|+c\overset{f(0)=0}{⇒}c=0\\
        f(x)&=-\ln|1-x|\quad x∈(-1,1)\\
        &=-\ln(1-x)
    \end{align*}
\end{itemize}
\vspace{10pt}

\underline{Beispiel:} Exponentialfunktion

$$\sum_{k=0}^∞\frac{1}{k!}x^k$$

Konvergenzintervall

$$\left|\frac{\frac{1}{(k+1)!}x^{k+1}}{\frac{1}{k!}x^k}\right|=\frac{k!}{(k+1)!}·|x|=\frac{1}{k+1}·|x|\overset{k→∞}{→}0=q(x)<1$$

d.h. $I=ℝ$ (Konvergenzradius ∞)
\vspace{10pt}

Summenfunktion $exp:\;ℝ→ℝ$

\begin{align*}
    exp(x)&=\sum_{k=0}^∞\frac{x^k}{k!}\quad\text{(Exponentialfunktion)}\\
    exp(0)&=\sum_{k=0}^∞\frac{0^k}{k!}=1\\
    exp(1)&=\sum_{k=0}^∞\frac{1^k}{k!}=\sum_{k=0}^∞\frac{1}{k!}=e\\
    exp(x+y)&=\sum_{n=0}^∞\frac{(x+y)^n}{n!}\\
    &=\sum_{n=0}^∞\frac{1}{n!}\sum_{k=0}^n\underset{→\frac{n!}{k!(n-k)!}}{\begin{pmatrix}
        n\\
        k
    \end{pmatrix}}x^ky^{n-k}\\
    &=\sum_{n=0}^∞\sum_{k=0}^n\frac{x^k}{k!}·\frac{y^{n-k}}{(n-k)!}\\
    &=\sum_{k=0}^∞\frac{x^k}{k!}·\sum_{k=0}^∞\frac{y^k}{k!}=exp(x)·exp(y)
\end{align*}

\begin{align*}
    exp(x)&=e^x\\
    e^{x+y}&=e^x·e^y\\
    (e^x)^n&=e^{nx}
\end{align*}

\end{document}